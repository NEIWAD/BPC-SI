package tests;

import com.efrei.model.Etudiant;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleTest {

        @Test
        public void newEtudiantStageIsNullTestCase() {
            Etudiant tester = new Etudiant();
            assertEquals(null, tester.getStage());
        }
}

docker stop tomcat-dev || true
docker run --name tomcat-dev --net=isolated_network -d -it --rm -p 4200:8080 tomcat:8.0
docker exec -d tomcat-dev rm -rf /usr/local/tomcat/webapps/*
docker cp /var/devops/dev/projectJeeV3-1.0-SNAPSHOT.war tomcat-dev:/usr/local/tomcat/webapps/devops.war
docker stop tomcat-release || true
docker run --name tomcat-release --net=isolated_network -d -it --rm -p 4200:8080 tomcat:8.0
docker exec -d tomcat-release rm -rf /usr/local/tomcat/webapps/*
docker cp /var/devops/release/projectJeeV3-1.0-SNAPSHOT.war tomcat-release:/usr/local/tomcat/webapps/devops.war